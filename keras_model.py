from keras import Sequential
from keras.layers import Masking
from keras.layers import LSTM
from keras.layers import Dense
from keras.layers import Dropout
from keras.regularizers import l1_l2
from keras.preprocessing import sequence
from keras.optimizers import Adam
from keras.preprocessing.sequence import pad_sequences
from Model_inputs import extract_datas
from Model_inputs import PaddedDataIterator,SimpleDataIterator
import matplotlib.pyplot as plt
import sys
import numpy as np

def acc_predict(results,label,n_classes):
    scores=[]
    ratio=0
    result=np.copy(results)
    for i,item in enumerate(result):
        max_list=[]
        for k in range(n_classes):
            M=list(item).index(max(item))
            max_list.append(M)
            item[M]=0
        if np.argmax(label[i]) in max_list:ratio+=1
        scores.append(max_list)

    return ratio*100/len(label)



(X_train,y_train),(X_test,y_test)=extract_datas()
nb_samples=len(X_train)
max_len=32
num_classes=len(y_train[0])

X_train = pad_sequences(X_train, maxlen=max_len)
X_train=np.reshape(X_train,(len(X_train),max_len,1))

X_test = pad_sequences(X_test, maxlen=max_len)
X_test=np.reshape(X_test,(len(X_test),max_len,1))

optimizer=Adam(lr=0.01)
n_hidden=64
model=Sequential()
model.add(Masking(mask_value=0.0))
model.add(LSTM(n_hidden,input_shape=(max_len,1)))
model.add(Dropout(0.2))
model.add(Dense(n_hidden,activity_regularizer=l1_l2(l1=0.0001,l2=0.0001)))
model.add(Dropout(0.3))
model.add(Dense(num_classes, activation='softmax'))
model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy'])
history=model.fit(X_train, y_train, validation_data=(X_test, y_test), epochs=20, batch_size=512,verbose=1)

print(model.summary())
scores = model.evaluate(X_test, y_test, verbose=0)
print(" Testing Accuracy: %.2f%%" % (scores[1] * 100))

scores = model.evaluate(X_train, y_train, verbose=0)
print(" Training Accuracy: %.2f%%" % (scores[1] * 100))

result=model.predict(X_test)
ratio=[]
for i in range(0,100,10):
     ratio.append(acc_predict(result,y_test,i))


# summarize history for accuracy
plt.plot(history.history['acc'])
plt.plot(history.history['val_acc'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()
# summarize history for loss
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()

plt.plot([i for i in range(0,100,10)],ratio)
plt.title('Accuracy % by number of cim10 returned')
plt.ylabel('Accuracy %')
plt.xlabel('Nb of cim10 returned')
plt.show()

# serialize model to JSON
model_json = model.to_json()
with open("model.json", "w") as json_file:
    json_file.write(model_json)
# serialize weights to HDF5
model.save_weights("model.h5")
print("Saved model to disk")



