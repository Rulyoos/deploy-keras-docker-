# deploy-keras-docker-(Dkd)

Dkd is a module you can clone to export any keras model with keras. For now, main features are predicting , training and versionning your IA across time.  
```bash
├── app
│   ├── app.py
│   ├── core
│   ├── Dockerfile
│   ├── __init__.py
│   ├── imports.py
│   ├── inputs.py
│   ├── models
│   │   ├── model_v0
│   │   │   ├── model.h5
│   │   │   └── model.json
│   │   ├── model_v1
│   │   │   ├── model.h5
│   │   │   └── model.json
│   │   ├── classes.json
│   │   ├── config.json
│   │   ├── delete.json
│   │   ├── last_predict.json
│   │   ├── log
│   │   └── train_pattern.json 
│   ├── requirements.txt
│   ├── sock.sock
│   ├── static
│   │   └── readme.html
│   └── uwsgi.ini
├── docker-compose.yml
├── nginx
│   ├── nginx.conf
│   └── uwsgi_params

```


## Requirements

Docker or docker tool-box is necessary to use this module. Download it [here](https://docs.docker.com/install/overview/). If you're using linux , do not forget to download docker-compose.

Python 3 is necessary too, download python3 [here](https://www.python.org/downloads/).

## Installation

Clone this project to a repository. 


```bash
clone https://github.com/Rulioos/deploy-keras-docker-.git
```

All you have to do now is to complete empty functions that are unique to your project and you're done.


## Usage

Insert a first trained model and change hyperparameters according to your project. 

### Windows

Launch Docker quickstart terminal then go in cmd.

```cmd
cd path/to/cloned/directory
docker-compose up
```
Then wait until the project builds completly into a docker container. Then check on  http://localhost/ in your browser.

If it does not work, check in your quick start terminal and get your docker machine ip. Then replace 0.0.0.0 or localhost by that ip.

Run this to get your machine ip
```cmd
docker-machine ip dev
```

### Linux 

```bash
cd path/to/cloned/directory
docker-compose up
```

Then wait until the project builds completly into a docker container. Then check on or http://localhost in your browser.

## Features
## Free configure


```json
{  
  "last_update": [  "31/10/18"  ],  
  "tag": "lastest",  
  "versions": [  0  ],  
  "n_classes": 40  
}
```
The config file is used for choosing version you'll use in predict, train. You can also choose how many classes your model will render if it's a classifier. It shall not work for regression.
You can also delete versions.


#### GET
To get the config via API, run
```bash
curl --request GET localhost/config
```
#### PUT

You can modify the number of classes you need to render. In the function it shall not be inferior to 0 or superior to the number of classes you use.

 - n_classes < 0 or n_classes>2035
 - tag == number of existing version

```bash
curl "localhost/config" \ 
--header "Content-Type: application/json" \
--request PUT \
--data '{"tag":"lastest","n_classes":20}' 


curl "localhost/config" \ 
--header "Content-Type: application/json" \
--request PUT \
--data '{"tag":"0","n_classes":60}' 

```

#### DELETE

You can delete versions by deleting them in the config file. Trying to delete a non-existing version will raise an error.
```bash

curl "localhost/config" \ 
--header "Content-Type: application/json" \
--request DELETE \
--data '  {"versions":[1,3,4,5]}' 

```




## Predict

```bash
curl "localhost/predict" \ 
--header "Content-Type: application/json" \
--request POST \
--data '{  "Examples":[Exemple1_json,Exemple2_json,...]}' 
```
Returns a JSON containing the response for each example
```json
{"0":
Exemple1_json
}
{"1":
Exemple2_json
}
```
## Training
The next request allows you to train and save your trained model.
```bash
curl "localhost/train" \ 
--header "Content-Type: application/json" \
--request POST \
--data '{  "Examples":[Exemple1,Exemple2,....]}' 
```
The request train and save a new model. Then it should return this.
```json
{" Testing Accuracy":" 100.00%"," Training Accuracy:":"100.00%","version":2}
```
## Log file
A log file registers updates,deletes and training. You can get log info here.
```bash
curl "localhost/getlog" --request GET
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

