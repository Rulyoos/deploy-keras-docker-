from imports import *



def parse_datas(json_datas, type):
    """
    Transform JSON file into
    :param json_datas: dictionnary  containing the datas to train or predict
    :param type: train or predict
    :return: list ready for predict or train
    """
    try:

        nb_samples = len(json_datas['Examples'])
        labels = []
        ex = []
        for example in json_datas['Examples']:
            """PARSE YOUR DATAS HERE"""

        max_len = 32
        if type == 'train':

            s = nb_samples * 80 // 100
            X_train, X_test = ex[:s], ex[s:]
            y_train, y_test = labels[:s], labels[s:]

            return (X_train, y_train, X_test, y_test), max_len

        else:
            return ex, max_len





    except KeyError as e:
        return 'Datas missing key:', e


def load_model():
    """
    Load the last model version
    :return: keras model
    """
    #    load json and create model
    config = parse_config_json()
    if config['tag']=="lastest":
        lastest_version = config['versions'][-1]
    else:
        try:
            lastest_version=config['versions'][int(config['tag'])]
        except IndexError or TypeError or ValueError:
            lastest_version = config['versions'][-1]


    json_path = 'models/model_v{}/model.json'.format(lastest_version)
    weight_path = 'models/model_v{}/model.h5'.format(lastest_version)
    json_file = open(json_path, 'r')
    loaded_model_json = json_file.read()
    json_file.close()

    model = model_from_json(loaded_model_json)
    # load weights into new model
    model.load_weights(weight_path)
    model._make_predict_function()
    print("Loaded model from disk")
    global graph
    graph = tf.get_default_graph()
    model._make_predict_function()

    return model


def acc_predict(results, n_classes, label=None, type='training'):
    """

    :param results:scores of predict function
    :param label:labels for training
    :param n_classes:number of classes to return 
    :return:a list of predictions for predict or an accuracy for training
    """
    if label is None and type is 'training':
        return "If you wish to train please give some labels"
    scores = []
    ratio = 0
    result = np.copy(results)
    for i, item in enumerate(result):
        max_list = []
        for k in range(n_classes):
            M = list(item).index(max(item))
            max_list.append(M)
            item[M] = 0
        if type is "training" and np.argmax(label[i]) in max_list: ratio += 1
        scores.append(max_list)
    if type is 'training':
        return ratio * 100 / len(label)
    else:
        m=dict()
        with open('models/classes.json', 'r') as outfile:
            classes=json.load(outfile)
        for i,k in enumerate(scores):
            m[i]=dict()
            for c,item in enumerate(k):
                m[i][c]=classes[str(item)]

        return m


def update_config(version_num):
    """Update config file to implement the list of versions and their updates"""

    your_time_zone='Pacific/Noumea'
    config = parse_config_json()
    now = datetime.datetime.now(pytz.timezone(your_time_zone))
    now.isoformat()
    config['last_update'].append(str(now))
    config['versions'].append(version_num)
    with open('models/config.json', 'w+') as outfile:
        json.dump(config, outfile)

    return jsonify(config)


def train_model(model, datas):
    """
        Train a model,keep it in memory and print scores
        :param model: a keras model
        :param json_training_datas:transformed input datas
        :return: a model and scores
    """

    (X_train, y_train, X_test, y_test), max_len = parse_datas(datas, type='train')
    X_train = pad_sequences(X_train, max_len, padding='post')
    X_train = np.reshape(np.array(X_train), (len(X_train), max_len, 1))
    X_test = pad_sequences(np.array(X_test), max_len, padding='post')
    X_test = np.reshape(np.array(X_test), (len(X_test), max_len, 1))

    y_train = np.array(keras.utils.to_categorical(y_test, num_classes=2035))
    y_test = np.array(keras.utils.to_categorical(y_test, num_classes=2035))

    #get config
    config = parse_config_json()
    # compile
    optimizer = keras.optimizers.Adam(lr=0.01)
    model.compile(loss="categorical_crossentropy", optimizer=optimizer, metrics=['accuracy'])

    with graph.as_default():
        testing=dict()
        model.fit(X_train, y_train, validation_data=(X_test, y_test), epochs=20, batch_size=512, verbose=1)
        scores = model.evaluate(X_test, y_test, verbose=0)
        testing [" Testing Accuracy"]=" %.2f%%" % (scores[1] * 100)
        conf_acc = scores[1] * 100

        scores = model.evaluate(X_train, y_train, verbose=0)
        testing[" Training Accuracy:"]= """%.2f%%""" % (scores[1] * 100)

        #commiting
        lastest_version = config['versions'][-1]
        lastest_version = config['versions'][-1]
        directory = 'models/model_v{}'.format(str(lastest_version + 1))
        update_config(lastest_version + 1)
        if not os.path.exists(directory): os.makedirs(directory)

        # serialize model to JSON
        model_json = model.to_json()
        with open(directory + "/model.json", "w") as json_file:
            json_file.write(model_json)  # serialize weights to HDF5
        model.save_weights(directory + "/model.h5")
        print("Saved model to disk")
        testing['version']=lastest_version+1

    keras.backend.clear_session()
    return testing


def predict_with_model(model, json_datas):
    datas, max_len = parse_datas(json_datas, type='predict')
    datas = pad_sequences(datas, max_len, padding='post')
    datas = np.reshape(np.array(datas), (len(datas), max_len, 1))
    with graph.as_default():
        results = model.predict(datas)

    keras.backend.clear_session()
    config=parse_config_json()
    n_classes=int(config['n_classes'])
    return acc_predict(results, type='predict', n_classes=n_classes)




def parse_config_json(path='models/config.json'):
    """
    Get dict from json file
    :param path: path to json file
    :return: dictionnary
    """
    with open(path, 'r') as conf:
        dict_conf = json.load(conf)
    return dict_conf

def get_config():
    """return config dict """
    config = parse_config_json('models/config.json')
    return config

def save_log(action):
    """save log into log file"""
    date=datetime.datetime.now(pytz.timezone(time_zone)).isoformat()
    with open("models/log", 'a') as j:
        j.write("{}: {} \n".format(date,action))
    return 0
