from functions import *

app = Flask(__name__,static_url_path='/static')


@app.route('/')
def welcome():
    """
    Print the readme for the basic url
    """
    return app.send_static_file('readme.html')


@app.route('/config', methods=['PUT', 'GET', "DELETE"])
def user_update_config():
    """
    PUT and GET method for the config file
    """
    ip_address=request.remote_addr #get ip address of the client
    if request.method == "PUT":

        """ PUT METHOD """
        tag=""
        n_classes=""
        JSON = request.json
        config = parse_config_json()
        for key, value in JSON.items():
            if key == "n_classes" and value in range(0, 2035):
                config["n_classes"] = value
                n_classes=str(value)
            if key == "tag":
                try:
                    if int(value) in [int(i) for i in config["versions"]]:
                        config["tag"] = str(value)
                        tag=str(value)
                    else:
                        pass
                except ValueError:
                    if value == "lastest":
                        config["tag"] = str(value)
                        tag=str(value)
                    else:
                        pass
        with open('models/config.json', 'w+') as outfile:
            json.dump(config, outfile)
            
        update_tag=("--tag changed to  "+ tag)*(tag!="")
        update_classes = ("--n_classes changed to  " + n_classes) * (n_classes!= "")
        log="UPDATE {} {} by {}".format(update_tag,update_classes,ip_address)
        save_log(log)
        return log

    if request.method == "GET":
        """ GET METHOD """

        return jsonify(get_config())

    if request.method == "DELETE":
        """ DEL METHOD """
        config = parse_config_json()
        JSON = request.json
        can_be_deleted = []
        for item in JSON['versions']:
            if int(item) != 0:
                try:
                    ind = config['versions'].index(item)
                    config['versions'].remove(item)
                    del config['last_update'][ind]
                    can_be_deleted.append(item)
                except KeyError and ValueError:
                    pass
        for num in can_be_deleted:
            folder_name = "models/model_v{}".format(num)
            shutil.rmtree(folder_name, ignore_errors=True)

        with open('models/config.json', 'w+') as outfile:
            json.dump(config, outfile)
            
            
        log="DELETE versions {} by {} ".format(can_be_deleted,ip_address)
        save_log(log)
        return log


@app.route('/train', methods=['POST'])
def train():
    ip_address=request.remote_addr #get ip address of the client
    if request.method == "POST":
        datas = request.json
        model = load_model()
        #return jsonify(datas)
        result = train_model(model=model, datas=datas)
        log="Trained model "+str(config['tag'])
        save_log("{} by {}".format(log,ip_address))
        return jsonify(result)


@app.route('/predict', methods=['POST'])
def predict():
    if request.method == 'POST':
        datas = request.json
        model = load_model()
        results = predict_with_model(model, datas)
        with open('models/last_predict.json', 'w') as f:
            json.dump(results, f)
    return jsonify(results)


@app.route('/getlast', methods=['GET'])
def getlast():
    with open('models/last_predict.json', 'r') as f:
        last = json.load(f)
    return jsonify(last)

@app.route('/getlog',methods=['GET'])
def getlog():
    with open("models/log","r") as file:
        data=file.read()
    return data


if __name__ == '__main__':
    app.run(debug=False, host='0.0.0.0')
